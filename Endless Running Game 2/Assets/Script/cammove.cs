﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cammove : MonoBehaviour
{

    public static cammove Instance = null;

    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 6);
    }


    void Update()
    {

    }

    public void Stop()
    {
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
    }
}

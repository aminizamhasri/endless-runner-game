﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public void mainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void gameScene()
    {
        SceneManager.LoadScene("Game");
    }
    public void login()
    {
        SceneManager.LoadScene("Login");
    }
    public void leaderboardScene()
    {
        SceneManager.LoadScene("Leaderboard");
    }
    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("Quit Game!");
    }

}
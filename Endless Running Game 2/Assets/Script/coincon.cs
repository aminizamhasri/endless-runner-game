﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class coincon : MonoBehaviour
{
    

    public static coincon Instance = null;

    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }
        
    void Start()
    {
        GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 5, 0);
    }

    
    void Update()
    {
        
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "dood")
        {
            gameflow.totalCoins += gameflow.Instance.AddedScore;

            Debug.Log(gameflow.totalCoins);
            //StartCoroutine(Tutorial.Instance.UpdateScore(gameflow.Instance.AddedScore));
            gameflow.Instance.Score.text = "Score: " + gameflow.totalCoins.ToString();
            Destroy(gameObject);
        }

        //GameManager.inst.IncrementScore();
        
    }

    

    public void Stop()
    {
        GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gameflow : MonoBehaviour
{
    public Transform tile1Obj;
    private Vector3 nextTileSpawn;
    public Transform bricksObj;
    private Vector3 nextBrickSpawn;
    private int randX;
    public Transform smCrateObj;
    private Vector3 nextSmCrateSpawn;
    public Transform dbCrateObj;
    private Vector3 nextDbCrateSpawn;
    public Transform cowObj;
    private Vector3 nextCowSpawn;
    private int randChoice;
    public static int totalCoins = 0;
    public Transform coinObj;
    public Text Score;
    public static gameflow Instance;

    public Transform powerup;
    private float powerupTimer = 7;

    public int AddedScore = 1;
    private bool isDoubleScore = false;
    public Text powerUpTimer;
    
    void Start()
    {
        nextTileSpawn.z = 27;
        StartCoroutine(spawnTile());

    }


    void Update()
    {

    }

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    IEnumerator spawnTile()
    {
        yield return new WaitForSeconds(1);
        randX = Random.Range(-1, 2);
        nextBrickSpawn = nextTileSpawn;
        nextBrickSpawn.y = -.2f;
        nextBrickSpawn.x = randX;
        Instantiate(tile1Obj, nextTileSpawn, tile1Obj.rotation);
        Instantiate(bricksObj, nextBrickSpawn, bricksObj.rotation);

        
        nextTileSpawn.z += 9;
        randX = Random.Range(-1, 2);
        nextSmCrateSpawn.z = nextTileSpawn.z;
        nextSmCrateSpawn.y = -.176f;
        nextSmCrateSpawn.x = randX;
        Instantiate(tile1Obj, nextTileSpawn, tile1Obj.rotation);
        Instantiate(smCrateObj, nextSmCrateSpawn, smCrateObj.rotation);

        if (randX == 0)
        {
            randX = 1;
        }
        else if (randX == 1)
        {
            randX = -1;
        }
        else
        {
            randX = 0;
        }

        randChoice = Random.Range(0, 10);
        if (randChoice == 0)
        {
            nextDbCrateSpawn.z = nextTileSpawn.z;
            nextDbCrateSpawn.y = -.151f;
            nextDbCrateSpawn.x = randX;
            Instantiate(dbCrateObj, nextDbCrateSpawn, dbCrateObj.rotation);
        }
        else if (randChoice == 1)
        {
            nextCowSpawn.z = nextTileSpawn.z;
            nextCowSpawn.y = -.47f;
            nextCowSpawn.x = randX;
            Instantiate(cowObj, nextCowSpawn, cowObj.rotation);
        }
        else if (randChoice == 2)
        {
            nextCowSpawn.z = nextTileSpawn.z;
            nextCowSpawn.y = .1f;
            nextCowSpawn.x = randX;
            Instantiate(powerup, nextCowSpawn, powerup.rotation);
        }
        else
        {
            nextCowSpawn.z = nextTileSpawn.z;
            nextCowSpawn.y = .1f;
            nextCowSpawn.x = randX;
            Instantiate(coinObj, nextCowSpawn, coinObj.rotation);
        }
        

        nextTileSpawn.z += 9;
        StartCoroutine(spawnTile()); // infinte loop
    }

    public void ActivateDoubleScore()
    {
        AddedScore = 2;
        StartCoroutine(DoubleScore());
    }

    IEnumerator DoubleScore()
    {
        powerupTimer -= Time.deltaTime;
        powerUpTimer.text = "PowerUp Timer: " + powerupTimer.ToString("n0");

        while (powerupTimer >= 0)
        {
            powerupTimer -= Time.deltaTime;
            powerUpTimer.text = "PowerUp Timer: " + powerupTimer.ToString("n0");
            yield return null;
        }

        //yield return new WaitForSeconds(7);

        AddedScore = 1;
        powerupTimer = 7;
    }
}

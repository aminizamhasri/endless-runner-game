﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverScreen : MonoBehaviour
{
    public Text pointsText;

    public void Setup()
    {
        gameObject.SetActive(true);
        //Time.timeScale = 0f;
        pointsText.text = gameflow.totalCoins.ToString() + " POINTS";
        StartCoroutine(Tutorial.Instance.UpdateScore(gameflow.totalCoins));
    }

    public void RestartButton()
    {
        SceneManager.LoadScene("Game");
    }

    public void mainMenuButton()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void leaderBoardButton()
    {
        SceneManager.LoadScene("Leaderboard");
    }
}

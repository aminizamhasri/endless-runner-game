﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class runningdood : MonoBehaviour
{
    private string laneChange = "n";
    private string midJump = "n";

    [SerializeField]
    private Text distanceText;
    private float distance = 0;
    private float addedDistance;
    public bool Test=false;
    public Animator anim;

    //public GameOver gameover;
    public GameOverScreen GameOverScreen;

    void Start()
    {
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 6);
        addedDistance = 1;
        Debug.Log(GetComponent<Rigidbody>().velocity);
    }

    
    void Update()
    {
        if (Test == true)
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 6);
            Test = false;
        }

        if ((Input.GetKey("a"))&& (laneChange=="n")&& (transform.position.x > -.9)) 
        {
            GetComponent<Rigidbody>().velocity = new Vector3(-1, 0, 6);
            laneChange = "y";
            StartCoroutine(stopLaneCh());
        }

        if ((Input.GetKey("d"))&& (laneChange=="n")&& (transform.position.x < .9))
        {
            GetComponent<Rigidbody>().velocity = new Vector3(1, 0, 6);
            laneChange = "y";
            StartCoroutine(stopLaneCh());
        }

        if ((Input.GetKey("space"))&& (midJump=="n"))
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, 2, 6);
            midJump = "y";
            StartCoroutine(stopJump());
        }

        if(Input.GetKey("p"))
        {
            SceneManager.LoadScene("Game");
        }

        distance = distance + addedDistance;
        distanceText.text = "Distance: " + distance.ToString();        
                
    }

    IEnumerator stopJump()
    {
        yield return new WaitForSeconds(.75f);
        GetComponent<Rigidbody>().velocity = new Vector3(0, -2, 6);
        yield return new WaitForSeconds(.75f);
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 6);
        midJump = "n";
    }

    IEnumerator stopLaneCh()
    {
        yield return new WaitForSeconds(1);
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 6);
        laneChange = "n";
        //Debug.Log(GetComponent<Transform>().position);
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "obstacle")
        {
            Debug.Log("ouch!");
            GameOverScreen.Setup();
            addedDistance = 0;
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            cammove.Instance.Stop();
            coincon.Instance.Stop();

            anim.enabled = false;
        }
        if (other.tag == "powerup")
        {
            gameflow.Instance.ActivateDoubleScore();
            Destroy(other.gameObject);
        }
                
    }
        
}

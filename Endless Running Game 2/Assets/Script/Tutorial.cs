﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    const string URI = "http://api.tenenet.net";
    const string token = "598f845164e2fb42dab697049abba0ae";
    
    public delegate void gs(LeaderboardJSON data);
    public delegate void scoreGs(ScoreJSON data);
    public static Tutorial Instance;

    void Start()
    {
        //StartCoroutine(registerNewPlayer());
        //StartCoroutine(getPlayer(alias));
        Debug.Log(PlayerPrefs.GetInt("score"));
    }

    public void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    void Update()
    {

    }


    public IEnumerator registerNewPlayer(string alias, string id, string fname, string lname, Text status)
    {
        UnityWebRequest www = UnityWebRequest.Get(URI + "/createPlayer" + "?token=" + token + "&alias=" + alias + "&id=" + id + "&fname=" + fname + "&lname=" + lname);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }

        Response resp = new Response();
        resp = JsonUtility.FromJson<Response>(www.downloadHandler.text);

        if (resp.message == "player_exists")
        {
            status.text = "Alias has been taken";
        }
        else
        {
            status.text = "Player is Registered!";
        }

        RegisterLogin.Instance.resetInput();

    }

    public IEnumerator getPlayer(string alias, Text status)
    {
        UnityWebRequest www = UnityWebRequest.Get(URI + "/getPlayer" + "?token=" + token + "&alias=" + alias);

        yield return www.SendWebRequest();

        Response resp = new Response();
        resp = JsonUtility.FromJson<Response>(www.downloadHandler.text);

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }

        if (resp.message == "player_no_exists")
        {
            status.text = "Player do not exist. Please register.";
        }
        else
        {
            PlayerPrefs.SetString("alias", alias);
            Debug.Log(alias);
            StartCoroutine(GetScoren(alias));
           
        }

    }

    public IEnumerator UpdateScore(int score)
    {
        string alias = PlayerPrefs.GetString("alias");

        Debug.Log(alias);
        int tt = PlayerPrefs.GetInt("score");
        if (score <= tt)
        {
            score = 0;
        }
        else
        {
            score = score - tt;
        }

        UnityWebRequest www = UnityWebRequest.Get(URI + "/insertPlayerActivity" + "?token=" + token + "&alias=" + alias + "&id=" + "metric22" + "&operator=add" + "&value=" + score);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {

            Debug.Log(www.error);
        }
        else
        {

            Debug.Log(www.downloadHandler.text);
        }
    }

    public IEnumerator GetScoren(string alias)
    {
        UnityWebRequest www = UnityWebRequest.Get(URI + "/getPlayer" + "?token=" + token + "&alias=" + alias);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {

            Debug.Log(www.error);
        }
        else
        {

            Debug.Log(www.downloadHandler.text);
        }
        Debug.Log("df");
        ScoreJSON resp = new ScoreJSON();
        resp = JsonUtility.FromJson<ScoreJSON>(www.downloadHandler.text);
        Debug.Log(resp.message.score[0].value);
        PlayerPrefs.SetInt("score", int.Parse(resp.message.score[0].value));
        SceneManager.LoadScene(1);
    }

    public IEnumerator GetScore(string alias, scoreGs data)
    {
        UnityWebRequest www = UnityWebRequest.Get(URI + "/getPlayer" + "?token=" + token + "&alias=" + alias);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {

            Debug.Log(www.error);
        }
        else
        {

            Debug.Log(www.downloadHandler.text);
        }

        ScoreJSON resp = new ScoreJSON();
        resp = JsonUtility.FromJson<ScoreJSON>(www.downloadHandler.text);
        data(resp);
        Debug.Log(resp.message.score[0]);
        
    }

    public IEnumerator GetLeaderboard(gs data)
    {
        UnityWebRequest www = UnityWebRequest.Get(URI + "/getLeaderboard" + "?token=" + token + "&id=" + "leaderboard22");

        yield return www.SendWebRequest();

        LeaderboardJSON resp = new LeaderboardJSON();
        resp = JsonUtility.FromJson<LeaderboardJSON>(www.downloadHandler.text);

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            data(resp);
        }
    }
}

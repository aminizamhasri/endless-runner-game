﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class RegisterLogin : MonoBehaviour
{
    public InputField aliasRegister;
    public InputField inputfname;
    public InputField inputlname;
    public InputField inputId;
    public InputField aliasLogin;

    public GameObject registerUI;
    public GameObject loginUI;

    public Text status;

    public static RegisterLogin Instance;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
               
    }

    public void registerButton()
    {
        StartCoroutine(Tutorial.Instance.registerNewPlayer(aliasRegister.text, inputId.text, inputfname.text, inputlname.text, status));
    }

    public void loginButton()
    {
        StartCoroutine(Tutorial.Instance.getPlayer(aliasLogin.text, status));
    }

    public void resetInput()
    {
        aliasRegister.text = "";        
        inputfname.text = "";
        inputlname.text = "";
        inputId.text = "";
        aliasLogin.text = "";
    }

    public void switchButton()
    {
        if (registerUI.activeInHierarchy == true)
        {
            registerUI.SetActive(false);
            loginUI.SetActive(true);

            resetInput();
        }
        else if (loginUI.activeInHierarchy == true)
        {
            loginUI.SetActive(false);
            registerUI.SetActive(true);

            resetInput();
        }
    }

}
